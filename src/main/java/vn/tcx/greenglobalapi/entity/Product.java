package vn.tcx.greenglobalapi.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int productId;
	
	@Column(name = "name", nullable = false)
	private String productName;
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OrderBy(clause = "imageMain DESC")
	private List<Image> images;
	
	@Column(name = "price", nullable = false)
	private double productPrice;
	
	@Column(name = "size", length = 10)
	private String productSize;
	
	@Column(name = "quantity", nullable = false)
	private int productQuantity;
	
	@Column(name = "description", columnDefinition="TEXT")
	private String productDescription;
	
	@Column(name = "status")
	private boolean productStatus;
	
	@Column(name = "featured")
	private boolean productFeatured;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "category_id", nullable = false)
	@JsonIgnore
	private Category category;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "manufacturer_id", nullable = false)
	@JsonIgnore
	private Manufacturer manufacturer;

}
