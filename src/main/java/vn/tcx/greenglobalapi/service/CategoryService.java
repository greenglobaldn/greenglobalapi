package vn.tcx.greenglobalapi.service;

import java.util.List;
import java.util.Optional;

import vn.tcx.greenglobalapi.entity.Category;

public interface CategoryService {

	List<Category> findAllCategory();
	
	Category getCategoryById(int categoryId);
	
	boolean existsCategoryById(int categoryId);
	
	Optional<Category> findCategoryById(int categoryId);
	
	Category saveCategory(Category category);
	
	void deleteCategory(Category category);
	
	Category updateCategory(int categoryId, Category categoryUpdated);
}
