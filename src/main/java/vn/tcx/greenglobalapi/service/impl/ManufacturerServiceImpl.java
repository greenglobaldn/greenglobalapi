package vn.tcx.greenglobalapi.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcx.greenglobalapi.entity.Manufacturer;
import vn.tcx.greenglobalapi.exception.NotFoundException;
import vn.tcx.greenglobalapi.repository.ManufacturerRepository;
import vn.tcx.greenglobalapi.service.ManufacturerService;

@Service
@Transactional
public class ManufacturerServiceImpl implements ManufacturerService{

	@Autowired
	private ManufacturerRepository manufacturerRepository;
	
	@Override
	public List<Manufacturer> findAllManufacturer() {
		return manufacturerRepository.findAll();
	}

	@Override
	public Manufacturer saveManufacturer(Manufacturer manufacturer) {
		return manufacturerRepository.save(manufacturer);
	}

	@Override
	public void deleteManufacturer(Manufacturer manufacturer) {
		manufacturerRepository.delete(manufacturer);
	}

	@Override
	public Manufacturer getManufacturerById(int manufacturerId) {
		return manufacturerRepository.getOne(manufacturerId);
	}

	@Override
	public Optional<Manufacturer> findManufacturerById(int manufacturerId) {
		return manufacturerRepository.findById(manufacturerId);
	}

	@Override
	public boolean existsManufacturerById(int manufacturerId) {
		return manufacturerRepository.existsById(manufacturerId);
	}

	@Override
	public Manufacturer updateManufacturer(int manufacturerId, Manufacturer manufacturerUpdated) {

		Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId)
				.orElseThrow(() -> new NotFoundException("Manufacturer not found with id " + manufacturerId));
		
		manufacturer.setManufacturerName(manufacturerUpdated.getManufacturerName());

		return manufacturerRepository.save(manufacturer);
	}

}
