package vn.tcx.greenglobalapi.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import vn.tcx.greenglobalapi.dto.ProductDTO;
import vn.tcx.greenglobalapi.dto.ProductImageDTO;
import vn.tcx.greenglobalapi.entity.Product;
import vn.tcx.greenglobalapi.excel.XlsxBuilder;
import vn.tcx.greenglobalapi.exception.NotFoundException;
import vn.tcx.greenglobalapi.repository.ProductRepository;
import vn.tcx.greenglobalapi.service.ProductService;
import vn.tcx.greenglobalapi.service.mapper.ProductMapper;
import vn.tcx.greenglobalapi.service.mapper.ProductMapping;

@Service
public class ProductServiceImpl implements ProductService {
	private static final String FILE_NAME_DANH_SACH_SAN_PHAM = "DanhSachSanPham.xlsx";

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> findAllProductAdmin() {
		return productRepository.findAll();
	}
	
	@Override
	public List<ProductImageDTO> findAllProduct() {
		return ProductMapping.productsToProductImageDTOs(productRepository.findAll());
	}

	@Override
	public long countByProductCategory(Product productCategory) {
		return productRepository.countByProductCategory(productCategory);
	}

	@Override
	public List<Product> searchProductByProductName(String name) {
		return productRepository.searchProductByProductName(name);
	}

	@Override
	public void deleteProuct(Product product) {
		productRepository.delete(product);
	}

	@Override
	public Product saveProduct(Product product) {
		return productRepository.save(product);
	}
	
    // Lấy các sản phẩm mới nhất
    @Override
    public List<ProductImageDTO> lastestProducts(boolean productStatus, boolean productFeatured, int index) {
    	return ProductMapping.productsToProductImageDTOs(productRepository.lastestProducts(productStatus, productFeatured, index));
    }

	// Lấy các sản phẩm nổi bật
	@Override
	public List<ProductImageDTO> featuredProducts(boolean productStatus, int index) {
		return ProductMapping.productsToProductImageDTOs(productRepository.featuredProducts(productStatus, index));
	}

	@Override
	public Product getByProductId(int productId) {
		return productRepository.findById(productId)
				.orElseThrow(() -> new RuntimeException());
	}
	
	@Override
	public ProductImageDTO getByProductImageIdDTO(int productId) {
		return ProductMapping.productToProductImageDTO(productRepository.findById(productId)
				.orElseThrow(() -> new RuntimeException()));
	}

	@Override
	public Optional<Product> findProductById(int productId) {
		return productRepository.findById(productId);
	}

	@Override
	public List<Product> findByCategoryId(int categoryId) {
		return productRepository.findByCategoryId(categoryId);
	}

	@Override
	public List<Product> findByManufacturerId(int manufacturerId) {
		return productRepository.findByManufacturerId(manufacturerId);
	}

	@Override
	public boolean existsByProductId(int productId) {
		return productRepository.existsById(productId);
	}

	// API Thêm sản phẩm
	@Override
	public Product createProduct(ProductDTO productDTO) {
		
		Product product = ProductMapper.INSTANCE.productDTOToProduct(productDTO);

		productRepository.save(product);

		return product;
	}

	// API Sửa sản phẩm
	@Override
	public Product updateProduct(int productId, ProductDTO productDTO) {

		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new NotFoundException("Product not found with id " + productId));

		productDTO.setProductId(productId);
		product = ProductMapper.INSTANCE.productDTOToProduct(productDTO);

		return productRepository.save(product);
	}

	@Override
	public ResponseEntity<byte[]> exportExcel(List<Product> products) {

		// Create header for excel
		List<String> tableHeaderName = new ArrayList<>();
		tableHeaderName.add("STT");
		tableHeaderName.add("Id");
		tableHeaderName.add("Tên sản phẩm");
		tableHeaderName.add("Mô tả sản phẩm");
		tableHeaderName.add("Kích thước");
		tableHeaderName.add("Giá");
		tableHeaderName.add("Số lượng");
		tableHeaderName.add("Danh mục");
		tableHeaderName.add("Nhà cung cấp");
		tableHeaderName.add("Trạng thái");

		// Create data for excel
		Object[][] data = new Object[products.size()][tableHeaderName.size()];
		for (int i = 0; i < products.size(); i++) {
			data[i][0] = i + 1;

			data[i][1] = products.get(i).getProductId();

			data[i][2] = products.get(i).getProductName() != null ? products.get(i).getProductName() : "";
			data[i][3] = products.get(i).getProductDescription() != null ? products.get(i).getProductDescription() : "";
			data[i][4] = products.get(i).getProductSize() != null ? products.get(i).getProductSize() : "";
			data[i][5] = products.get(i).getProductPrice();

			data[i][6] = products.get(i).getProductQuantity();

			data[i][7] = products.get(i).getCategory().getCategoryName() != null
					? products.get(i).getCategory().getCategoryName()
					: "";
			data[i][8] = products.get(i).getManufacturer().getManufacturerName() != null
					? products.get(i).getManufacturer().getManufacturerName()
					: "";
			data[i][9] = products.get(i).isProductStatus();

		}

		byte[] excel = new XlsxBuilder().startSheet("Danh sách sản phẩm")
				.setContent("Danh sách sản phẩm ", tableHeaderName, data).build();

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM_VALUE))
				.header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + FILE_NAME_DANH_SACH_SAN_PHAM + "\"")
				.body(excel);

	}

	@Override
	public void exportCSV(HttpServletResponse response) {

		// set file name and content type
		String fileName = "DanhSachSanPham.csv";

		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");

		// create a csv writer
		StatefulBeanToCsv<ProductDTO> writer;
		try {
			writer = new StatefulBeanToCsvBuilder<ProductDTO>(response.getWriter())
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
					.withSeparator(CSVWriter.DEFAULT_SEPARATOR)
					.withOrderedResults(true).build();
			writer.write(ProductMapper.INSTANCE.productsToProductDTOs(productRepository.findAll()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public ResponseEntity<byte[]> exportWord() throws Exception {
		String fileName = "DanhSachSanPham.docx";

		// create a document
		XWPFDocument document = new XWPFDocument();
		XWPFParagraph title = document.createParagraph();
		title.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun titleRun = title.createRun();
		titleRun.setText("Thống Kê");
//		titleRun.setColor("009933");
		titleRun.setBold(true);
		titleRun.setFontFamily("Time New Roman");
		titleRun.setFontSize(20);

		XWPFParagraph subTitle = document.createParagraph();
		subTitle.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun subTitleRun = subTitle.createRun();
		subTitleRun.setText("Thống kê chi tiết");
//		subTitleRun.setColor("00CC44");
		subTitleRun.setBold(true);
		subTitleRun.setFontFamily("Time New Roman");
		subTitleRun.setFontSize(16);
		subTitleRun.setTextPosition(20);
		subTitleRun.setUnderline(UnderlinePatterns.SINGLE);

		XWPFParagraph image = document.createParagraph();
		image.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun imageRun = image.createRun();
		imageRun.setTextPosition(20);

		File fileImage = new ClassPathResource("static/img/logo.png").getFile();
		imageRun.addPicture(Files.newInputStream(fileImage.toPath()), XWPFDocument.PICTURE_TYPE_PNG,
				fileImage.toPath().getFileName().toString(), Units.toEMU(100), Units.toEMU(100));

		XWPFParagraph sectionTitle = document.createParagraph();
//		sectionTitle.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun sectionTRun = sectionTitle.createRun();
		sectionTRun.setText("Thông tin");
//		sectionTRun.setColor("00CC44");
		sectionTRun.setBold(true);
		sectionTRun.setFontFamily("Time New Roman");
		sectionTRun.setFontSize(13);
		sectionTRun.setTextPosition(20);
		sectionTRun.setUnderline(UnderlinePatterns.SINGLE);

		// Paragraph 1
		XWPFParagraph para1 = document.createParagraph();

		// first line indentation in the paragraph
		para1.setIndentationFirstLine(400);
		// left alignment
		para1.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun para1Run = para1.createRun();
		int sizeListProduct = productRepository.findAll().size();
		para1Run.setText("Tổng số sản phẩm hiện có trong cửa hàng: " + sizeListProduct);

//		// Paragraph 2
//		XWPFParagraph para2 = document.createParagraph();
//		
//		// left alignment
//		para2.setAlignment(ParagraphAlignment.LEFT);
//		// wrap words
//		para2.setWordWrap(true);
//		
//		XWPFRun para2Run = para2.createRun();
//		para2Run.setText("Tổng số sản phẩm: " );

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		document.write(baos);
		baos.close();
		document.close();
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM_VALUE))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
				.body(baos.toByteArray());
	}

}
