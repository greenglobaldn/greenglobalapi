package vn.tcx.greenglobalapi.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcx.greenglobalapi.entity.Image;
import vn.tcx.greenglobalapi.entity.Product;
import vn.tcx.greenglobalapi.exception.NotFoundException;
import vn.tcx.greenglobalapi.repository.ImageRepository;
import vn.tcx.greenglobalapi.repository.ProductRepository;
import vn.tcx.greenglobalapi.service.ImageService;

@Service
@Transactional
public class ImageServiceImpl implements ImageService{

	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
//	private final Path fileStorageLocation;
//	
//	@Autowired
//	public ImageServiceImpl(FileStorageProperties fileStorageProperties) {
//		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
//		
//		try {
//			Files.createDirectories(this.fileStorageLocation);
//		} catch (Exception e) {
//			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", e);
//		}
//	}
//	
//	@Override
//	public String storeFile(MultipartFile file) {
//		
//		// Normalize file name
//		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		
//		try {
//			// Check if  the file's name contains invalid characters
//			if (fileName.contains("..")) {
//				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
//			}
//			
//			// Copy file to the target location (Replacing existing file with the same name)
//			Path targetLocation = this.fileStorageLocation.resolve(fileName);
//			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
//			
//			return fileName;
//		} catch (Exception e) {
//			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
//		}
//	}
//	
//	@Override
//	public Resource loadFileAsResource(String fileName) {
//		
//		try {
//			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
//			Resource resource = new UrlResource(filePath.toUri());
//			
//			if (resource.exists()) {
//				return resource;
//			} else {
//				throw new MyFileNotFoundException("File not found " + fileName);
//			}
//		} catch (MalformedURLException e) {
//			throw new MyFileNotFoundException("File not found " + fileName, e);
//		}
//	}
//	
//	@Override
//	public Image getImage(int imageId) {
//		return imageRepository.findById(imageId)
//				.orElseThrow(() -> new MyFileNotFoundException("File not found with id " + imageId));
//	}
	
	@Override
	public Image saveImage(Image image) {
		return imageRepository.save(image);
	}
	
	@Override
	public void deleteImage(int imageId) {
		imageRepository.deleteById(imageId);
	}

	@Override
	public Image getByImageId(int imageId) {
		return imageRepository.getOne(imageId);
	}

	@Override
	public List<Image> findAllImages() {
		return imageRepository.findAll();
	}

	@Override
	public Optional<Image> findImageById(int imageId) {
		return imageRepository.findById(imageId);
	}

	@Override
	public void deleteImage(Image image) {
		imageRepository.delete(image);
	}

	@Override
	public List<Image> findByProductId(int productId) {
		return imageRepository.findByProductId(productId);
	}

	@Override
	public Image createImage(int productId, Image image) {

		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new NotFoundException("Product not found with id " + productId));
		
		image.setProduct(product);
		
		return imageRepository.save(image);
	}

	@Override
	public Image upadateImage(int productId, int imageId, Image imageUpdated) {

		if(!productRepository.existsById(productId)) {
			throw new NotFoundException("Product not found!");
		}
		
		Image image = imageRepository.findById(imageId)
				.orElseThrow(() -> new NotFoundException("Image not found with id " + imageId));
		
		image.setImageName(imageUpdated.getImageName());
		image.setImageMain(imageUpdated.isImageMain());
		image.setImageStatus(imageUpdated.isImageStatus());
		
		return imageRepository.save(image);
	}
}
