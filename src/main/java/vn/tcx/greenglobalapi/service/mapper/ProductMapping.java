package vn.tcx.greenglobalapi.service.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import vn.tcx.greenglobalapi.dto.ProductImageDTO;
import vn.tcx.greenglobalapi.entity.Product;

public class ProductMapping {
	
	private ProductMapping() {
	    throw new IllegalStateException("Utility class");
	  }

	public static List<ProductImageDTO> productsToProductImageDTOs(List<Product> products) {
        if ( products == null ) {
            return null;
        }

        List<ProductImageDTO> list = new ArrayList<>( products.size() );
        for ( Product product : products ) {
            list.add( productToProductImageDTO( product ) );
        }

        return list;
    }

    public static ProductImageDTO productToProductImageDTO(Product product) {
        if ( product == null ) {
            return null;
        }

        ProductImageDTO productImageDTO = new ProductImageDTO();

        productImageDTO.setProductId( product.getProductId() );
        productImageDTO.setProductName( product.getProductName() );
        productImageDTO.setImages( product.getImages() );
        productImageDTO.setImage( !product.getImages().isEmpty() ? product.getImages().get(0).getImageName() : StringUtils.EMPTY);
        productImageDTO.setProductPrice( product.getProductPrice() );
        productImageDTO.setProductSize( product.getProductSize() );
        productImageDTO.setProductQuantity( product.getProductQuantity() );
        productImageDTO.setProductDescription( product.getProductDescription() );
        productImageDTO.setProductStatus( product.isProductStatus() );
        productImageDTO.setProductFeatured( product.isProductFeatured() );

        return productImageDTO;
    }
}
