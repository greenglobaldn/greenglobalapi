package vn.tcx.greenglobalapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.tcx.greenglobalapi.entity.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer>{

	@Query(value = "SELECT * FROM image WHERE product_id = :product_id", nativeQuery = true)
	List<Image> findByProductId(@Param("product_id") int productId);

}
