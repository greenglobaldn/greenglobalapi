package vn.tcx.greenglobalapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.tcx.greenglobalapi.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{

}
