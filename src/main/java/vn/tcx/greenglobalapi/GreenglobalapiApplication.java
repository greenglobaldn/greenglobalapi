package vn.tcx.greenglobalapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"vn.tcx.greenglobalapi.*"})
 	
public class GreenglobalapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenglobalapiApplication.class, args);
	}

}
