package vn.tcx.greenglobalapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDTO {
	
	private int productId;
	
	private String productName;

	private double productPrice;

	private String productSize;

	private int productQuantity;

	private String productDescription;

	private boolean productStatus;

	private boolean productFeatured;

	private int categoryId;

	private int manufacturerId;
}
