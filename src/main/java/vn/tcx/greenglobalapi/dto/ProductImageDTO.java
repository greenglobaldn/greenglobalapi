package vn.tcx.greenglobalapi.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import vn.tcx.greenglobalapi.entity.Image;

@Getter
@Setter
public class ProductImageDTO {
	
	private int productId;
	
	private String productName;
	
	private String image;
	
	private List<Image> images;
	
	private double productPrice;
	
	private String productSize;
	
	private int productQuantity;
	
	private String productDescription;
	
	private boolean productStatus;
	
	private boolean productFeatured;
}
