package vn.tcx.greenglobalapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import vn.tcx.greenglobalapi.entity.Image;
import vn.tcx.greenglobalapi.exception.NotFoundException;
import vn.tcx.greenglobalapi.service.ImageService;
import vn.tcx.greenglobalapi.service.ProductService;

@RestController
@RequestMapping("/api/v1/admin/products/{productId}/images")
@Tag(name = "Image")
public class ImageProductRestController {

	@Autowired
	private ImageService imageService;
	
	@Autowired
	private ProductService productService;
	
	@GetMapping
	public List<Image> getAllImageByProductId(@PathVariable int productId){
		 
		if(!productService.existsByProductId(productId)) {
			throw new NotFoundException("Product not found!");
		}
		
		return imageService.findByProductId(productId);
	}
	
	@PostMapping
	public Image createImage(@PathVariable int productId,
							@RequestBody Image image) {
		
		try {
			imageService.createImage(productId, image);
		} catch (Exception e) {
			throw new NotFoundException("Product not found with id " + productId);
		}
		
		return image;
	}
	
	@PutMapping("/{imageId}")
	public Image upadateImage(@PathVariable int productId,
							@PathVariable int imageId,
							@RequestBody Image imageUpdated) {
		
		Image image = null;
		
		try {
			image = imageService.upadateImage(productId, imageId, imageUpdated);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		
		return image;
	}
	
	@DeleteMapping("/{imageId}")
	public String deleteImage(@PathVariable int productId,
							@PathVariable int imageId) {
		
		if(!productService.existsByProductId(productId)) {
			throw new NotFoundException("Product not found!");
		}
		
		Image image = imageService.findImageById(imageId)
				.orElseThrow(() -> new NotFoundException("Image not found with id " + imageId));
		
		imageService.deleteImage(image);
		
		return "Deleted Successfully!";
	}
	
}
