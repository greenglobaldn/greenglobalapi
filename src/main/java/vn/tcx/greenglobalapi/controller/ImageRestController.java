package vn.tcx.greenglobalapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import vn.tcx.greenglobalapi.entity.Image;
import vn.tcx.greenglobalapi.service.ImageService;

@RestController
@RequestMapping("/api/v1/admin/images")
@Tag(name = "Image")
public class ImageRestController {

	@Autowired
	private ImageService imageService;
	
	@GetMapping
	public List<Image> findAllImages(){
		
		return imageService.findAllImages();
	}
	
}
