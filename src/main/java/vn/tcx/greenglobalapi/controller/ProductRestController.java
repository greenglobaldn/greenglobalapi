package vn.tcx.greenglobalapi.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import vn.tcx.greenglobalapi.dto.ProductDTO;
import vn.tcx.greenglobalapi.entity.Product;
import vn.tcx.greenglobalapi.exception.NotFoundException;
import vn.tcx.greenglobalapi.service.CategoryService;
import vn.tcx.greenglobalapi.service.ManufacturerService;
import vn.tcx.greenglobalapi.service.ProductService;

@RestController
@RequestMapping(value = "/api/v1/admin")
@Tag(name = "Product")
public class ProductRestController {

	@Autowired
	private ProductService productService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ManufacturerService manufacturerService;

	@Operation(description = "Xem danh sách Product", responses = {
            @ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = Product.class))), responseCode = "200") })
    @ApiResponses(value = {
            @ApiResponse(responseCode  = "200", description = "Thành công"),
            @ApiResponse(responseCode  = "401", description = "Chưa xác thực"),
            @ApiResponse(responseCode  = "403", description = "Truy cập bị cấm"),
            @ApiResponse(responseCode  = "404", description = "Không tìm thấy")
    })
	@GetMapping("/products")
	public ResponseEntity<List<Product>> findAllProducts() {
		
		List<Product> products = productService.findAllProductAdmin();
		
		if(products.isEmpty()) {
			//You many decide to return HttpStatus.NOT_FOUND
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}

	@GetMapping("/categories/{categoryId}/products")
	public List<Product> getAllProductByCategoryId(@PathVariable int categoryId) {

		if (!categoryService.existsCategoryById(categoryId)) {
			throw new NotFoundException("Category not found!");
		}

		return productService.findByCategoryId(categoryId);
	}

	@GetMapping("/manufacturers/{manufacturerId}/products")
	public List<Product> getAllProductByManufacturerId(@PathVariable int manufacturerId) {

		if (!manufacturerService.existsManufacturerById(manufacturerId)) {
			throw new NotFoundException("Manufacturer not found!");
		}

		return productService.findByManufacturerId(manufacturerId);
	}

	@GetMapping(value = "/products/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> getProductById(@PathVariable("productId") int productId) {

		Optional<Product> product = productService.findProductById(productId);

		if (!product.isPresent()) {
			System.out.println("Không tìm thấy Product với Id: " + productId);
			return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok().body(product.get());
	}
	
	@PostMapping("/products")
	public ResponseEntity<Product> createProduct(@Valid @RequestBody ProductDTO productDTO) {

		Product product = null;

		try {
			product = productService.createProduct(productDTO);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return ResponseEntity.ok().body(product);
	}

//	@RequestParam(required = true) int categoryId,
//	@RequestParam(required = true) int manufacturerId,
	@PutMapping(value = "/products/{productId}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> updateProduct(@PathVariable("productId") int productId, 
												@Valid @RequestBody ProductDTO productDTO) {

		Product product = null;

		try {
			product = productService.updateProduct(productId, productDTO);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}

		return ResponseEntity.ok().body(product);
	}

	@DeleteMapping("/products/{productId}")
	public String deleteProduct(@PathVariable int productId) {

		Product product = productService.findProductById(productId)
				.orElseThrow(() -> new NotFoundException("Product not found with id " + productId));

		productService.deleteProuct(product);

		return "Deleted Successfully!";
	}

	@GetMapping("/products/downloads/excel")
	public ResponseEntity<byte[]> exportExcel() {
		List<Product> products = productService.findAllProductAdmin();
		return productService.exportExcel(products);
	}
	
	@GetMapping("/products/downloads/csv")
	public void exportCSV(HttpServletResponse response) {
		productService.exportCSV(response);
	}
	
	@GetMapping("/products/downloads/word")
	public ResponseEntity<byte[]> exportWord() throws Exception {
		return productService.exportWord();
	}
	
}
