package vn.tcx.greenglobalapi.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import vn.tcx.greenglobalapi.entity.Category;
import vn.tcx.greenglobalapi.exception.NotFoundException;
import vn.tcx.greenglobalapi.service.CategoryService;

@RestController
@RequestMapping(value = "/api/v1/admin/categories")
@Tag(name = "Category")
public class CategoryRestController {

	@Autowired
	private CategoryService categoryService;
	
	@GetMapping
	public ResponseEntity<List<Category>> findAllCategories(){
		
		List<Category> categories = categoryService.findAllCategory();
		
		if(categories.isEmpty()) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Category> createCategory(@Valid @RequestBody Category category) {
		
		categoryService.saveCategory(category);
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping(value = "/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Category> getCategoryById(@PathVariable int categoryId){
		
		Optional<Category> category = categoryService.findCategoryById(categoryId);
		
		if (category.isPresent()) {
			System.out.println("Không tìm thấy Category với Id: " + categoryId);
			return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
		} 
		
		return ResponseEntity.ok().body(category.get());
	}
	
	@PutMapping(value = "/{categoryId}",
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Category updateCategory(@PathVariable("categoryId") int categoryId, @RequestBody Category categoryUpdated){
		
		Category category = null;
		
		try {
			category = categoryService.updateCategory(categoryId, categoryUpdated);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		
		return category;
	}
	
	@DeleteMapping("/{categoryId}")
	public String deleteCategory(@PathVariable int categoryId) {
		
		Category category = categoryService.findCategoryById(categoryId)
				.orElseThrow(() -> new NotFoundException("Category not found with id " + categoryId));
			
		categoryService.deleteCategory(category);
		
		return "Deleted Successfully!";
			
	}
	
}
